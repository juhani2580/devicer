module.exports = {
  development: {
    client: "sqlite3",
    connection: {
      filename: "./dev.sqlite3"
    },
    useNullAsDefault: true,
    migrations: {
      directory: "./db/migration",
      tableName: "knex_migration"
    },
    seeds: {
      directory: "./db/seed"
    }
  },
  staging: {
    client: "sqlite3",
    connection: {
      filename: "./dev.sqlite3"
    },
    useNullAsDefault: true,
    migrations: {
      directory: "./db/migration",
      tableName: "knex_migration"
    },
    seeds: {
      directory: "./db/seed"
    }
  },
  production: {
    client: "sqlite3",
    connection: {
      filename: "./dev.sqlite3"
    },
    useNullAsDefault: true,
    migrations: {
      directory: "./db/migration",
      tableName: "knex_migration"
    },
    seeds: {
      directory: "./db/seed"
    }
  }
};
