import { observable } from "mobx";
import { RouterStore } from "mobx-react-router";

export default observable({
  router: new RouterStore(),
  auth: {
    auth: "pending"
  },
  device: {
    table: []
  },
  deviceType: {
    table: []
  },
  software: {
    table: []
  },
  deviceSoftware: {
    table: []
  },
  user: {
    table: []
  }
});
