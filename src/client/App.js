import React from "react";
import { observable } from "mobx";
import { observer, inject } from "mobx-react";
import { Switch, Route, Redirect } from "react-router-dom";
import { CssBaseline, colors } from "@material-ui/core";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import Login from "./component/Login";
import NavBar from "./component/NavBar";
import Devices from "./component/Devices";
import AddDevice from "./component/AddDevice";
import DeviceTypes from "./component/DeviceTypes";
import AddDeviceType from "./component/AddDeviceType";
import DeviceSoftwares from "./component/DeviceSoftwares";
import Softwares from "./component/Softwares";
import Reservations from "./component/Reservations";
import Users from "./component/Users";
import AddUser from "./component/AddUser";
import Settings from "./component/Settings";
import { DeviceTable, SoftwareTable, UserTable } from "./component/table";
import axios from "./axios";

let theme = createMuiTheme({
  palette: {
    primary: colors.blue.A900
  }
});

theme.overrides = {};

@inject("router", "auth")
@observer
class App extends React.Component {
  async componentDidMount() {
    let { auth } = this.props;
    let res = (await axios.get("auth")).data;
    if (res.error) {
      auth.auth = "false";
    } else {
      auth.auth = "true";
    }
  }

  render() {
    let { classes, router, auth } = this.props;
    let { location, push, goBack } = router;
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <div>
          {(() => {
            switch (auth.auth) {
              case "false":
                return <Login />;
              case "true":
                return (
                  <div>
                    <NavBar />
                    <main style={{ marginTop: 96 }}>
                      <Switch>
                        <Route
                          exact
                          path="/"
                          component={() => <div>etusivu</div>}
                        />
                        <Route path="/device" component={DeviceTable} />
                        <Route path="/software" component={SoftwareTable} />
                        <Route path="/settings" component={Settings} />
                        <Route path="/reservation" component={Reservations} />
                        <Route path="/user" component={UserTable} />
                        <Route render={() => <h1>404</h1>} />
                      </Switch>
                    </main>
                  </div>
                );
              case "pending":
              default:
                return null;
            }
          })()}
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
