import React from "react";
import { Link } from "react-router-dom";
import { observer, inject } from "mobx-react";
import { AppBar, Toolbar, Button } from "@material-ui/core";
import * as icon from "@material-ui/icons";
import axios from "../axios";

let NavButton = props => <Button {...props} variant="raised" size="small" />;

@inject("auth")
@observer
class NavBar extends React.Component {
  render() {
    let { auth } = this.props;
    return (
      <div>
        <AppBar>
          <Toolbar>
            <NavButton component={Link} to="/device">
              <icon.Devices />
              devices
            </NavButton>
            <NavButton component={Link} to="/software">
              <icon.Apps />
              software
            </NavButton>
            <NavButton component={Link} to="/user">
              <icon.People />
              users
            </NavButton>
            <NavButton component={Link} to="/reservation">
              <icon.Event />
              reservations
            </NavButton>
            <NavButton component={Link} to="/settings">
              <icon.Settings />
              settings
            </NavButton>
            <NavButton
              onClick={async e => {
                let res = (await axios.post("logout")).data;
                if (res.error) {
                  throw res.error;
                }
                auth.auth = "false";
              }}
            >
              log out
            </NavButton>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default NavBar;
