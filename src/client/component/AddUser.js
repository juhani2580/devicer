import React from "react";
import { Link } from "react-router-dom";
import { observable } from "mobx";
import { observer } from "mobx-react";
import {
  Button,
  TextField,
  Radio,
  RadioGroup,
  FormHelperText,
  FormLabel,
  FormControlLabel,
  FormControl,
  Paper
} from "@material-ui/core";
import axios from "../axios";

@observer
class AddUser extends React.Component {
  @observable username = "";
  @observable password = "";
  @observable showPassword = false;
  @observable accessRole = null;
  @observable accessRoles = [];
  async componentDidMount() {
    this.accessRoles = (await axios.get("access_role")).data;
  }
  render() {
    return (
      <div>
        <form>
          <TextField
            name="username"
            label="Username"
            onChange={event => {
              this.username = event.target.value;
            }}
          />
          <br />
          <TextField
            name="password"
            label="Password"
            type={!this.showPassword ? "password" : ""}
            onChange={event => {
              this.password = event.target.value;
            }}
          />
          <br />
          <TextField
            name="passwordConfirm"
            label="Password again"
            type={!this.showPassword ? "password" : ""}
          />
          <br />
          <FormControl component="fieldset" required>
            <FormLabel component="legend">Access role</FormLabel>
            <RadioGroup
              name="access-role"
              value={this.accessRole}
              onChange={event => {
                this.accessRole = event.target.value;
              }}
            >
              {this.accessRoles.map(a => (
                <FormControlLabel
                  key={a.id}
                  value={a.id.toString()}
                  control={<Radio />}
                  label={a.value}
                />
              ))}
            </RadioGroup>
          </FormControl>
          <br />
          <Button
            variant="raised"
            onClick={() => {
              this.showPassword = !this.showPassword;
            }}
          >
            {!this.showPassword ? "Show" : "Hide"} password
          </Button>
          <Button
            variant="raised"
            color="primary"
            onClick={async () => {
              await axios.post("register", {
                username: this.username,
                password: this.password,
                access_role_id: this.accessRole
              });
            }}
          >
            Register
          </Button>
        </form>
      </div>
    );
  }
}

export default AddUser;
