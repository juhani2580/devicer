import React from "react";
import { observer, inject } from "mobx-react";
import GenericTable from "./GenericTable";
import schemas from "../../common/schemas";
import DeviceForm from "./DeviceForm";

export let DeviceTable = inject("device")(
  observer(({ device }) => (
    <GenericTable
      table={schemas.device.table}
      store={device}
      schema={schemas.device}
      Form={DeviceForm}
    />
  ))
);

export let SoftwareTable = inject("software")(
  observer(({ software }) => (
    <GenericTable
      table={schemas.software.table}
      store={software}
      schema={schemas.software}
      Form={() => <div />}
    />
  ))
);

export let UserTable = inject("user")(
  observer(({ user }) => (
    <GenericTable
      table={schemas.user.table}
      store={user}
      schema={schemas.user}
      Form={() => <div />}
    />
  ))
);
