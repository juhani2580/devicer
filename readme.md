# Digitalents Device Registry

## Setup

```
npm i
knex migrate:latest
```

### Init database

1. Move CSV files to `db/seed/data`
2. `knex seed:run`

## Running

```
npm run dev
```
